const serializer = (payload, format, customized) => {

  const data = {
    timestamp: new Date(),
    location: undefined,
    format: format,
    customized: customized,
  }

  if (customized) {
    data.customizationConfig = payload.customizationConfig
    data.customizationConfig.size = payload.exportSize
  }
  data.icons = payload.icons

  return data

}

const postAnalyticsData = (mongoClient, iconsJson, exportType, customized, requestHost) => {
  const isRequestFromProduction = requestHost === process.env.FRONTEND_HOST;

  if (process.env.NODE_ENV === "production" && isRequestFromProduction) {
    const analyticsData = serializer(iconsJson, exportType, customized)
    postDataToDb(mongoClient, analyticsData)
  }

  return;
}

const postDataToDb = (mongoClient, payload) => {
  let response = null;
  try {
    response = mongoClient.db("analytics").collection("icons-picker").insertOne(payload)
  }
  catch(err) {
    console.log(err)
    return {"success": false}
  }
  finally {
    response.then( (res) => console.log(`Document ID: ${res.insertedId}`))
    return {"success": true}
  }

}
module.exports =  {serializer, postDataToDb, postAnalyticsData};
